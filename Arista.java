public class Arista{
	
	private int id;
	private String nombre;
	private String prop;


	public Arista(int i, int j, int k){

		id = i;
		nombre = "A" + i;
		prop = j + " -- " + k + "\n";

	}

	public int getId(){
		return id;
	}

	public String getNombre(){
		return nombre;
	}

	public String getProp(){
		return prop;
	}

	public void setProp(int i, int j){

		prop += i + " -- " + j + "\n";

	}

}