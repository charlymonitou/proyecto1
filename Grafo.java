import java.util.*;
import java.io.FileWriter;
import java.text.SimpleDateFormat;

public class Grafo{

		static HashMap<Vertice, HashSet<Vertice>> grafo = new HashMap<>();
		static HashSet<Arista> aristas = new HashSet<>();
		static int n,m,d;
		static int conta;
		static double prob, r;
		static Vertice [] nodos;
		static boolean auto;
		static boolean dirigido;
		
		public static void genGilbert(int nu, double p, boolean autod, boolean dirigidod) {

			auto = autod;
			dirigido = dirigidod;

			n = nu;
			prob = p;
			nodos = new Vertice[n];
			for(int i =0; i<n; i++){
			Vertice nod = new Vertice(i);
			nodos[i] = nod;
			grafo.put(nod, new HashSet<Vertice>());
			}

			Random rand = new Random();
			
			conta = 0;

			for (int i=0; i<n; i++){
				HashSet<Vertice> set = grafo.get(nodos[i]);
				for(int j=0; j<n; j++){

				if(rand.nextDouble() <= prob){

					autos(i,j);
				}
				}
			}
			crea(convierte(), "Gil");
		}

		public static void genErdosRenyi(int nu, int mu, boolean autod, boolean dirigidod){

			auto = autod;
			dirigido = dirigidod;

			n = nu;
			m = mu;

			nodos = new Vertice[n];
			for(int i =0; i<n; i++){
			Vertice nod = new Vertice(i);
			nodos[i] = nod;
			grafo.put(nod, new HashSet<Vertice>());
			}
		
			Random rand = new Random();
			Random rand2 = new Random();
			conta =0;

			while (conta < m){

				int n1 = rand.nextInt(n);
				int n2 = rand2.nextInt(n);

				autos(n1,n2);
			}
			crea(convierte(), "Erdos");
		}

		public static void genGeografico(int nu, double ru, boolean autod, boolean dirigidod){

			auto = autod;
			dirigido = dirigidod;

			n = nu;
			r = ru;

			nodos = new Vertice[n];

			Random c1 = new Random();
			Random c2 = new Random();

			for(int i =0; i<n; i++){
			double x = c1.nextDouble();
			double y = c2.nextDouble();
			Vertice nod = new Vertice(i, c1.nextDouble(), c2.nextDouble());
			nodos[i] = nod;
			grafo.put(nod, new HashSet<Vertice>());
			}

			conta = 0;
			for (int i=0; i<n; i++){
				Vertice set1 = nodos[i];
				for(int j= i; j<n; j++){
					Vertice set2 = nodos[j];
					double longitud = Math.hypot(Math.abs(set1.getX() - set2.getX()), Math.abs(set1.getY() - set2.getY()));

					if(r <= longitud){

						autos(i,j);
					}
				}
			}
				crea(convierte(), "Geografico");
			}

			public static void genBarabasiAlbert(int nu, int du, boolean autod, boolean dirigidod){

			auto = autod;
			dirigido = dirigidod;

			int numAr=0;

			n = nu;
			d = du;
			nodos = new Vertice[n];
			

			for(int i =0; i<n; i++){
			Vertice nod = new Vertice(i);
			nodos[i] = nod;
			grafo.put(nod, new HashSet<Vertice>());
			creav(i,d);
			}

    		crea(convierte(), "Barabasi");
			}



			private static void crea(StringBuffer s, String nom){

				StringBuffer datos = s;
				String nombre = nom;

				String hola = "graph Grafo1 {\n\n" + datos + "\n}";

				Date d1 = new Date();

				SimpleDateFormat s1 = new SimpleDateFormat("HH:mm:ss - dd-MM-yyyy");
		
				String nombref = s1.format(d1);

				FileWriter fichero = null;
				try {

					fichero = new FileWriter(nom + "/G_" + nombref +".gv");
				fichero.write(hola);
			
			fichero.close();
			System.out.println("Creado");

		}catch (Exception ex) {
			System.out.println("Mensaje de la excepción: " + ex.getMessage());
		}
	}

			private static boolean checa(int i, int j){

				Vertice n1 = nodos[i];
				Vertice n2 = nodos[j];

				HashSet<Vertice> set1 = grafo.get(nodos[i]);
				HashSet<Vertice> set2 = grafo.get(nodos[j]);

				if (set1.contains(n2) || set2.contains(n1)) return true;

				else return false;
			}

			private static StringBuffer convierte(){

				StringBuffer s = new StringBuffer();

				for (int i=0; i < n; i++){
					HashSet<Vertice> set = grafo.get(nodos[i]);
					if (set.isEmpty()){
							s.append(i + ";\n");
							//System.out.println("hola");
						}
					for(Vertice a : set){
						
						s.append(i + " -- " + a.getId() + ";\n");
					}
				}
				return s;
			}

			private static void conecta(int i, int j){

				HashSet<Vertice> set = grafo.get(nodos[i]);
				Vertice nodo = nodos[j];
				set.add(nodo);
				conta++;
				aristas.add(new Arista(conta,i,j));
			}

			private static void autos(int i, int j){

				if (auto && dirigido){		
							conecta(i,j);	
					}
					else if(auto){
							if(!checa(i,j)){
								conecta(i,j);
							}
					}
					else if(dirigido){
							if(i != j){
								conecta(i,j);
							}
					}
					else{
						if(i != j){
							if(!checa(i,j)){
							conecta(i,j);
							}
						}
					}
			}

			public static void creav(int nodoOrigen, int d){

				if (nodoOrigen ==0) return;
				conta =0;
				Random r1 = new Random();
				double proba =0;
				for (int nodoDestino = nodoOrigen - 1; conta < d  && nodoDestino >= 0; nodoDestino--){
					proba = 1 - ((double)conta/d);
					if(r1.nextDouble() <= proba){
						conecta(nodoOrigen, nodoDestino);
						//conta++;
					}

				}


			}
}