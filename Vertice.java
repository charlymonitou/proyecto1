public class Vertice{

	private String nombre;
	private int id;
	private int grado;
	private double coordx;
	private double coordy;
	

	public Vertice(int i){

		id = i;
		nombre = "V" + i;
		grado =0;
	}

	public Vertice(int i, double x, double y){

		id = i;
		nombre = "V" + i;
		grado =0;
		coordx = x;
		coordy = y;
	}

	public int getId(){
		return id;
	}

	public String getNombre(){
		return nombre;
	}

	public int getGrado(){
		return grado;
	}


	public double getX(){
		return coordx;
	}

	public double getY(){
		return coordy;
	}
	
	public void setGrado(int g){

		grado = g;

	}


}